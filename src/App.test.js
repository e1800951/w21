import React from "react";
import { configure, shallow, assert } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ComponentName from "./components/ComponentName";
import App from "./App";

configure({ adapter: new Adapter() });
describe("Should print text", () => {
  it("Returns Hello world", () => {
    const wrapper = shallow(<ComponentName />);
    expect(wrapper.find("div").text()).toEqual("Hello world!");
  });
});


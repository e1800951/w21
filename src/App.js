import logo from './logo.svg';
import './App.css';
import ComponentName from './components/ComponentName';

function App() {
  return (
    <ComponentName />
  );
}

export default App;
